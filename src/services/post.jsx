import axios from 'axios';

export function getPosts(query={}){
    return dispatch => {
        return axios({
            method: 'get',
            url: 'http://api.netskills.pl/post',
            params: query
            }); 
       
    };
           
}

export function addPost(data){
    return dispatch => {
        return axios.post('http://api.netskills.pl/post',data);
    };
}

export function deletePost(id){
    return dispatch => {
        return axios.delete(`http://api.netskills.pl/post/${id}`);
    };
}

export function updatePost(id, data){
    return dispatch => {
        return axios.put(`http://api.netskills.pl/post/${id}`,data);
    };
}