import React from 'react';
import _ from 'lodash';
import { validateLogin } from './../validators.jsx';
import TextFieldGroup from './../helpers/TextFieldGroup.jsx';
import { connect} from 'react-redux';
import { userLoginRequest, setUser } from './../services/user.jsx';
import setAuth from './../helpers/setAuth.jsx';
import jwt from 'jsonwebtoken';
import { executeResult } from './../helpers/executeResult.jsx';

/**
 * Login component
 */
class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            'name': '',
            'password': '',
            'errors': [],
            'isLoading': false
        };


        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    isValid() {
        const {errors, isValid} = validateLogin(this.state);
        if (!isValid) {
            this.setState({errors: errors});
        }
        return isValid;
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.isValid()) {
            this.setState({errors: [], isLoading: true});
            this.props.userLoginRequest({'name': this.state.name, 'password': this.state.password}).then(
                (result) => {
                  
                    
                    executeResult(result,this,(result,that) => {
                        const token = result.data.result.token;
                        localStorage.setItem('token',token);
                        console.log(localStorage.getItem('token'));
                        setAuth(token);
                        let user = jwt.decode(token.split(' ')[1]);
                        this.props.setUser(user);
                        that.props.history.push("/");
                    });

                },
                (result) => {
                    console.log(result);
                    this.setState({errors: ['Something wrong'], 'isLoading': false});
                }
            );
        }


    }

    render() {

        const errors = this.state.errors.map((val) => {
            return (<li key={val}>{val}</li>);
        });
        const  { isAuth} = this.props.auth;
        if(isAuth){
            this.props.history.push("/");
        }
        return (
                
                <div className="container" >
                    <div className="row">
                        <div className="col-md-6 col-md-offset-3">
                            <div className="panel panel-login">
                                <div className="panel-heading">
                                    <div className="row">
                                        <div className="col-xs-12">
                                            LOGIN
                                        </div>
                
                                    </div>
                                    <hr/>
                                </div>
                                <div className="panel-body">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="errors">
                                                <ul>
                                                    {errors}
                                                </ul>
                                            </div>      
                                            <form className="form-register" onSubmit={this.onSubmit} name="register">
                                                <TextFieldGroup
                                                    name="name"
                                                    id="name"
                                                    type="text"
                                                    value={this.state.name}
                                                    placeholder="Name"
                                                    onChange={this.onChange}
                                                    />
                                                <TextFieldGroup
                                                    name="password"
                                                    id="password"
                                                    type="password"
                                                    value={this.state.password}
                                                    placeholder="Password"
                                                    onChange={this.onChange}
                                                    />
                
                
                                                <div className="form-group">
                                                    <div className="row">
                
                                                        <div className="col-sm-6 col-sm-offset-3">
                                                            <input 
                                                                disabled={this.state.isLoading}
                                                                type="submit" name="register-submit" id="register-submit"
                                                                className="form-control btn btn-register"
                                                                value="Log In" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                );
    }
}

LoginForm.propTypes = {
    auth: React.PropTypes.object.isRequired,
 
};

function mapStateToProps(state){
    
    return {
        auth: state.auth
    };
}



export default connect(mapStateToProps, {userLoginRequest ,setUser})(LoginForm);

