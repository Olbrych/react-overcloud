import React from 'react';
import { connect } from 'react-redux';
import { validatePost } from './../validators.jsx';
import { getPosts, updatePost, addPost, deletePost } from './../services/post.jsx';
import Panel from './Panel.jsx';
import TextFieldGroup from './../helpers/TextFieldGroup.jsx';
import { executeResult } from './../helpers/executeResult.jsx';

/**
 * React component - App
 * get data from api
 * CRUD post
 */
class App extends React.Component {
    /**
     * contructor, init state, bind functions
     * @param {object} props
     * @returns {App}
     */
    constructor(props) {
        super(props);
        this.state = {
            posts: {},
            info: '',
            query: {
                'query': {},
                'limit': 3,
                'sort': {'created_at': -1}

            },
            title: '',
            text: '',
            isLoading: false,
            _id: false,
            header: 'New Post',
            errors: []

        };


        this.props.getPosts(this.state.query).then((result) => {
            let posts = {};
            _.forEach(result.data.result, (val) => {
                if (_.isObject(val)) {
                    posts[val._id] = val;
                }
            });
            this.setState({posts: posts});
        });

        this.saveP = this.saveP.bind(this);
        this.editP = this.editP.bind(this);
        this.deleteP = this.deleteP.bind(this);
        this.onChange = this.onChange.bind(this);
    }
    /**
     * set new empty state for post
     * @returns {void)
     */
    initPost() {
        this.setState({
            title: '',
            text: '',
            isLoading: false,
            _id: false,
            header: 'New Post',
            errors: []
        });
    }
    /**
     * validate post
     * @returns {boolean}
     */
    isValid() {
        const {errors, isValid} = validatePost(this.state);
        if (!isValid) {
            this.setState({errors: errors});
        }
        return isValid;
    }
    /**
     * check user is Auth
     * @returns {App.props.auth.isAuth}
     */
    isAuth() {
        console.log(this.props.auth.isAuth);
        return this.props.auth.isAuth;
    }
    ;
            /**
             * get user data
             * @returns {App.props.auth.user}
             */
            getUser() {
        return this.props.auth.user;
    }
    ;
    /**
     * create or update post
     * @param {event} e
     * @returns {void}
     */
    saveP(e) {
        e.preventDefault();
        if (this.isValid() && this.isAuth()) {
            this.setState({errors: [], isLoading: true});

            if (!_.isEmpty(this.state._id)) {

                this.props.updatePost(this.state._id, {'title': this.state.title, 'text': this.state.text}).then(
                        (result) => {

                    executeResult(result, this, function (result, that) {

                        that.state.posts[that.state._id] = result.data.result;
                        that.state.posts[that.state._id].user = that.getUser();
                        that.state.info = 'Success';
                        that.initPost();
                    });

                },
                (result) => {
                    this.setState({errors: ['Something wrong'], 'isLoading': false});
                });
            } else {
                console.log('create');
                this.props.addPost({'title': this.state.title, 'text': this.state.text}).then(
                        (result) => {
                    executeResult(result, this, function (result, that) {

                        that.state.posts[result.data.result._id] = result.data.result;
                        that.state.posts[result.data.result._id].user = that.getUser();
                        that.state.info = 'Success';
                        that.initPost();
                    });

                },
                (result) => {
                    this.setState({errors: ['Something wrong'], 'isLoading': false});
                });
            }


        }

    };
    /**
     * select post to update
     * @param {object} val
     * @returns {void}
     */
    editP(val) {

        this.setState({
            header: 'Edit post',
            _id: val._id,
            title: val.title,
            text: val.text
        });


    };
    /**
     * delete post
     * @param {object} val
     * @returns {void}
     */
    deleteP(val) {
        const {isAuth, user} = this.props.auth;

        if (isAuth && user._id === val.user._id) {
            this.props.deletePost(val._id).then((result) => {

                delete this.state.posts[val._id];
                this.setState({posts: this.state.posts});
            });
        }
    };
    
    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }
    /**
     * render component
     * @returns {String|Boolean|Array|App.render.panels}
     */
    render() {

        const {isAuth, user} = this.props.auth;
        const errors = this.state.errors.map((val) => {
            return (<li key={val}>{val}</li>);
        });

        let panels = [];
        _.forEach(this.state.posts, (val, key) => {
            panels.push(<Panel key={val._id} post={val} isAuth={isAuth} user={user} 
            editFunction={this.editP} deleteFunction={this.deleteP} />);
        });


        return (
                <div className="container main-content" >
                    <div className="col-md-8" >
                        {panels}
                    </div>
                    { this.isAuth() &&
                    <div className="col-md-2" >
                        <div id="post-panel">
                            <h5>
                                {this.state.info}
                            </h5>
                            <h3>{this.state.header}</h3>
                            <ul>
                                {errors}
                            </ul>

                            <form name="ff" id="ff" role="form" onSubmit={this.saveP}>
                                <TextFieldGroup
                                    name="title"
                                    id="title"
                                    type="text"
                                    value={this.state.title}
                                    placeholder="Title"
                                    onChange={this.onChange}
                                    />
                                <textarea
                                    name="text"
                                    placeholder="Text"
                                    onChange={this.onChange}
                                    value={this.state.text}
                                    ></textarea>
                                <div className="form-group">
                                    <input type="submit" name="login-submit" 
                                           disabled={this.state.isLoading}

                                           id="login-submit" tabIndex="4" className="form-control btn btn-login" value="SAVE" />
                                </div>
                            </form>
                        </div>
                    </div>
                    }
                
                
                </div>
                );
    }
}

App.propTypes = {
    auth: React.PropTypes.object.isRequired

};

function mapStateToProps(state) {
    return {
        auth: state.auth
    };
}

export default connect(mapStateToProps, {getPosts, addPost, updatePost, deletePost})(App);