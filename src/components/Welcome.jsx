import React from 'react';
import { connect } from 'react-redux';

class Welcome extends React.Component {
    
    render() {
        const { isAuth, user} = this.props.auth;
        return (<div>   
                { isAuth && 
                <div className="alert alert-success">
                    Hi, {user.name}
                </div>
                }
                {!isAuth &&
                     <div className="alert alert-warning">
                     Hi, Guest
                    </div>
                }
                </div>
               );
    }
}

Welcome.propTypes = {
    auth: React.PropTypes.object.isRequired,
 
};

function mapStateToProps(state){
    
    return {
        auth: state.auth
    };
}

export default connect(mapStateToProps,{})(Welcome);