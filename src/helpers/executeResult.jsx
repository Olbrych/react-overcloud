
export function executeResult(result, that, successCallback) {
    
    let errors = [];
    if (result.data.status === 1) {
        console.log('success');
        successCallback(result, that);
    } else if (result.data.status === 0) {
        if (result.data.error.hasOwnProperty('errors')) {
            _.forEach(result.data.error.errors, function (value) {
                errors.push(value.message);
            });
        }else {
            errors.push(result.data.error.message);
        }
    } else {
        errors.push('Something wrong');
    }
    if (errors.length) {
        that.setState({errors: errors});
    }
    that.setState({isLoading: false});
}