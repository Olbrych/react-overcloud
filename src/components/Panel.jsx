import React from 'react';

class Panel extends React.Component {
    
    editF(e){
        this.props.editFunction(e);
    };
    deleteF(e){
        this.props.deleteFunction(e);
    }
    render(){
        return (
            <div className="panel panel-info">
                <div className="panel-heading">
                <h3 className="panel-title panel-left">
                    {this.props.post.title}
                </h3>

                </div>
                <div className="panel-body">
                    {this.props.post.text}
                    <br/><br/>
                    Info:
                    <ul>
                        <li><span className="author">Author: {this.props.post.user.name}</span></li>
                        <li><span className="author">Created At: {this.props.post.created_at} </span></li>

                    </ul>

                </div>
                
                
                <div className="panel-footer">
                { this.props.isAuth && this.props.user._id === this.props.post.user._id &&     
                    <div>
                        <a href="#ff" onClick={()=>this.editF(this.props.post)}>EDIT</a> 
                        <a onClick={()=>this.deleteF(this.props.post)} style={{'marginLeft': '30px','cursor':'pointer'}} >DELETE</a>
                    </div>
                }
                </div>
                
            </div>
               );
    }
}


Panel.propTypes = {

    post: React.PropTypes.object.isRequired,
    isAuth: React.PropTypes.bool.isRequired,
    user: React.PropTypes.obejct,
    deleteFunction: React.PropTypes.func,
    editFunction: React.PropTypes.func
}


export default Panel;
