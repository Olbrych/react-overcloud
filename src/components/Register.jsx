import React from 'react';
import RegisterForm from './RegisterForm.jsx';
import { connect} from 'react-redux';
import { userRegisterRequest } from './../services/user.jsx';


class Register extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            user: ''
        }
        
        this.setUser = this.setUser.bind(this);
    }
  
    setUser(value){
        this.setState({user: value});
        
    }
    
    render() {
        const { userRegisterRequest } = this.props;
        const  { isAuth} = this.props.auth;
        if(isAuth){
            this.props.history.push("/");
        }
        return (
        
            <div className="container">
             <div className="row">
                    <div className="col-md-6 col-md-offset-3">
                        <div className="panel panel-login">
                            <div className="panel-heading">
                                <div className="row">
                                    <div className="col-xs-12">
                                        REGISTER
                                    </div>

                                </div>
                                <hr/>
                            </div>
                            {this.state.user.length > 0 && 
                            <div className="panel-body"  >
                                <div className="row">
                                    <div className="col-lg-12">
                                    <h1>Hi, {this.state.user}. Now You can log in.</h1>
                                    </div>
                                </div>
                            </div> }
                            {this.state.user.length === 0 &&
                            <div className="panel-body" >
                                <div className="row">
                                    <RegisterForm 
                                        userRegisterRequest={userRegisterRequest}
                                        setUser={this.setUser}
                                        />
                                </div>
                               
                            </div>
                             }
                        </div>
                    </div>
                </div>
            </div>
            );
    }
}

Register.propsTypes = {
    userRegisterRequest: React.PropTypes.func.isRequired,
     auth: React.PropTypes.object.isRequired,
};


function mapStateToProps(state){
    
    return {
        auth: state.auth
    };
}

export default connect(mapStateToProps,{userRegisterRequest})(Register);

        