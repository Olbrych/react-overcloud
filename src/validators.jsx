import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateRegister(data){
    let errors = [];
    if(Validator.isEmpty(data.name) || !Validator.isLength(data.name,{min: 3, max: 10}) ){
        errors.push('Username  - reqiured , min=3, max=10');
    }
    
    if(Validator.isEmpty(data.password) || !Validator.isLength(data.password,{min: 3, max: 10}) ){
        errors.push('Password  - reqiured , min=3, max=10, same=Confirm password');
    }
    
    if(Validator.isEmpty(data.confirm_password) || data.confirm_password !== data.password || 
           !Validator.isLength(data.confirm_password,{min: 3, max: 10}) ){
        errors.push('Confirm Password  - reqiured , min=3, max=10, same=password');
    }
    
    return {
        errors,
        isValid: isEmpty(errors)
    }
}

export function validateLogin(data){
    let errors = [];
    if(Validator.isEmpty(data.name)){
        errors.push('Username  - reqiured');
    }
    
    if(Validator.isEmpty(data.password)){
        errors.push('Password  - reqiured');
    }
    
    return {
        errors,
        isValid: isEmpty(errors)
    }
}


export function validatePost(data){
    let errors = [];
    if(Validator.isEmpty(data.title) || !Validator.isLength(data.title,{min: 3, max: 30})){
        errors.push('Title  - reqiured, min - 3, max = 30');
    }
    
    if(Validator.isEmpty(data.text)){
        errors.push('Text  - reqiured');
    }
    
    return {
        errors,
        isValid: isEmpty(errors)
    }
}
