import { SET_USER } from './../services/types.jsx';
import isEmpty from 'lodash/isEmpty';

const initialState = {
    isAuth: false,
    user: {}
}

export default (state= initialState, action ={})=>{
    console.log(action);
    switch(action.type){
        case SET_USER:
            return {
                isAuth: !isEmpty(action.user),
                user: action.user
            }
        default: return state;
    }
}