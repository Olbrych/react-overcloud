import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from './../services/user.jsx';

class Navbar extends React.Component {
    logout(e){
        e.preventDefault();
        this.props.logout();
    }
    render() {
        const { isAuth, user} = this.props.auth;
        
        
        return (
            <div>
                <div className="container-fluid">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span className="sr-only">MENU</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <a className="navbar-brand" href="#">MENU</a>
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        { isAuth &&
                            <ul className="nav navbar-nav">
                                <li>
                                    <Link to="/">Home</Link>
                                </li>
                                <li>
                                    <Link to="/logout" onClick={this.logout.bind(this)}>Logout</Link>
                                </li>
                            </ul> 
                        }
                        { !isAuth && 
                            <ul className="nav navbar-nav">
                                <li>
                                    <Link to="/">Home</Link>
                                </li>

                                <li>
                                    <Link to="/login">Log in</Link>
                                </li>
                                <li>
                                    <Link to="/register">Register</Link>
                                </li>

                            </ul>
                        }
                    </div>
                </div>
               
            </div>
            );
    }
}

Navbar.propTypes = {
    auth: React.PropTypes.object.isRequired,
    logout: React.PropTypes.func.isRequired
};

function mapStateToProps(state){
    
    return {
        auth: state.auth
    };
}

export default connect(mapStateToProps,{logout})(Navbar);

        