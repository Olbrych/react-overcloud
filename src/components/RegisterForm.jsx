import React from 'react';
import _ from 'lodash';
import { validateRegister }  from './../validators.jsx';
import TextFieldGroup from './../helpers/TextFieldGroup.jsx';
import { executeResult } from './../helpers/executeResult.jsx';


class RegisterForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            'name': '',
            'password': '',
            'confirm_password': '',
            'errors': [],
            'isLoading': false
        };

        
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e){
        this.setState({[e.target.name] : e.target.value });
    }

    isValid(){
        const { errors, isValid } = validateRegister(this.state);
        if(!isValid){
            this.setState({errors: errors});
        }
        return isValid;
    }


    onSubmit(e){
        e.preventDefault();
        if(this.isValid()){
            this.setState({errors: [], isLoading: true });
            this.props.userRegisterRequest({'name':this.state.name,'password':this.state.password}).then(
                    (result) => {
                        executeResult(result,this,function(result,that){
                            that.props.setUser(result.data.result.name);
                          });
//                        var errors = [];
//                        if(result.data.status === 1){
//                            this.props.setUser(result.data.result.name);
//                        }else if(result.data.status ===0){
//                            if (result.data.error.hasOwnProperty('errors')) {
//                                _.forEach(result.data.error.errors, function (value) {
//                                    errors.push(value.message);
//                                });
//                            }
//                        }else{
//                            errors.push('Something wrong');
//                        }
//                        if(errors.length){
//                            this.setState({errors: errors});
//                        }
//                        this.setState({isLoading: false});
                    },
                    (result) => {
                        this.setState({errors: ['Something wrong'],'isLoading': false});
                    }
            );
        }

        
    }

    render() {
        
        const errors = this.state.errors.map((val)=>{
            return (<li key={val}>{val}</li>);
        });
        return (
        <div className="col-lg-12">
          <div className="errors">
            <ul>
                {errors}
            </ul>
          </div>      
          <form className="form-register" onSubmit={this.onSubmit} name="register">
                <TextFieldGroup
                    name="name"
                    id="name"
                    type="text"
                    value={this.state.name}
                    placeholder="Name"
                    onChange={this.onChange}
                />
                <TextFieldGroup
                    name="password"
                    id="password"
                    type="password"
                    value={this.state.password}
                    placeholder="Password"
                    onChange={this.onChange}
                />
                <TextFieldGroup
                    name="confirm_password"
                    id="confirm_password"
                    type="password"
                    value={this.state.confirm_password}
                    placeholder="Confirm Password"
                    onChange={this.onChange}
                />
    
                <div className="form-group">
                    <div className="row">

                        <div className="col-sm-6 col-sm-offset-3">
                            <input 
                                disabled={this.state.isLoading}
                                type="submit" name="register-submit" id="register-submit"
                                className="form-control btn btn-register"
                                value="Register Now" />
                        </div>
                    </div>
                </div>
            </form>
             </div>
        );
    }
}
    
RegisterForm.propTypes = {
    userRegisterRequest: React.PropTypes.func.isRequired,
    setUser: React.PropTypes.func
};

export default RegisterForm;

        