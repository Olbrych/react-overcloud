import { combineReducers } from 'redux';

import auth from './reducers/auth.jsx';

export default combineReducers({
   auth     
});