import React from 'react';
import {render} from 'react-dom';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';

import Welcome from './components/Welcome.jsx';
import Navbar from './components/Navbar.jsx';
import Register from './components/Register.jsx';
import LoginForm from './components/LoginForm.jsx';
import App from './components/App.jsx';
import setAuth from './helpers/setAuth.jsx';
import jwt from 'jsonwebtoken';
import { setUser } from './services/user.jsx';
import rootReducer from './rootReducer.jsx';


const store = createStore(
    rootReducer,
    applyMiddleware(thunk)
);

/**
 * if exists token set data for user
 */
if(localStorage.getItem('token')){
    setAuth(localStorage.getItem('token'));
    store.dispatch(setUser(jwt.decode(localStorage.getItem('token').split(' ')[1])));

}
render(
   <Provider store={store}>
   <Router>
    <div>
    <Navbar />
    <Welcome />
        <div className="container">
            <Route exact path="/" component={App} />
            <Route path="/register" component={Register}  />
            <Route path="/login" component={LoginForm} />

        </div>
    </div>
  </Router>
  </Provider>
  , document.getElementById('app'));
