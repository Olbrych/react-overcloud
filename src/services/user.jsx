import axios from 'axios';
import { SET_USER } from './../services/types.jsx';
import setAuth from './../helpers/setAuth.jsx';
import jwt from 'jsonwebtoken';

export function setUser(user){
 
    return {
        type: SET_USER,
        user
    }
   
}

export function logout(){
    return dispatch => {
        localStorage.removeItem('token');
        setAuth(false);
        dispatch(setUser({}));
        
    }
}


export function userLoginRequest(data){
    return dispatch => {
        return axios.post('http://api.netskills.pl/auth',data);
    }
}


export function userRegisterRequest(userData){
    return dispatch => {
        return axios.post('http://api.netskills.pl/user',userData);
    }
}


//export function userLoginRequest(userData){
//    return dispatch => {
//        return axios.post('http://api.netskills.pl/auth',userData);
//    }
//}