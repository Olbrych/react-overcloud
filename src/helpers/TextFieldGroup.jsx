import React from 'react';

const TextFieldGroup = ({name,value,id, type,placeholder,onChange, error})=>{
    return (
            <div className="form-group">
            <input 
                onChange={onChange}
                value={value}
                id={id}
                type={type}
                name={name}
                placeholder={placeholder}
                className="form-control"
                />
            <span className="help-block">{error}</span>
            </div>
            );
};

TextFieldGroup.propTypes = {

    value: React.PropTypes.string.isRequired,
    type: React.PropTypes.string.isRequired,
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    error: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    onChange:React.PropTypes.func.isRequired
            
}

TextFieldGroup.defaultProps = {
    type: 'text'
}

export default TextFieldGroup;